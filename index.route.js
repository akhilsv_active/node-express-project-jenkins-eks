const express = require('express');
const userRoutes = require('./server/user/user.route');
const authRoutes = require('./server/auth/auth.route');

const router = express.Router(); // eslint-disable-line new-cap

// TODO: use glob to match *.route files

/** GET /health-check - Check service health */

/**MAIN ROUTE */
router.get('/', (req, res) =>
  res.send('<center><h2>Hello World!</h2><p>Build v2</p></center>')
);
/** Health Check Route */
router.get('/health-check', (req, res) =>
  res.send('Health Check......OK!\n')
);

// mount user routes at /users
router.use('/users', userRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

module.exports = router;
